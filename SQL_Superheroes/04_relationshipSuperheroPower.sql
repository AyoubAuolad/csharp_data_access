USE SuperheroesDb;

CREATE TABLE Superhero_Power (
    SuperheroId int not null,
    PowerId int not null,
);

ALTER TABLE Superhero_Power ADD FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id);
ALTER TABLE Superhero_Power ADD FOREIGN KEY (PowerId) REFERENCES Power(Id);
ALTER TABLE Superhero_Power ADD PRIMARY KEY (SuperheroId, PowerId);
