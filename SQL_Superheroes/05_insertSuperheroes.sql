USE SuperheroesDb;

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Deadpool', 'DPool', 'Marvels');

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Saitama', 'One Punch', 'One Punch Man');

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Wolverine', 'Wolf', 'XMen');