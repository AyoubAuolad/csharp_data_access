﻿namespace DataAccess.Models
{
    public class CustomerSpender
    {
        public string CustomerName { get; set; }
        public decimal Spend { get; set; }
    }
}