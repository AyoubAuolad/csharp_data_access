﻿namespace DataAccess.Models
{
    public class CustomerGenre
    {
        public string CustomerName { get; set; }
        public string MostPopularGenre { get; set; }
        public int NumberOfInvoices { get; set; }
    }
}