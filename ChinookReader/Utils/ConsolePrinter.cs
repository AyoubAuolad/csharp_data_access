﻿using System;
using System.Collections.Generic;
using DataAccess.Models;

namespace DataAccess.Utils
{
    public static class ConsolePrinter
    {
        /// <summary>
        /// Print a table of customers to the console.
        /// </summary>
        /// <param name="customers">Collection of Customers to print.</param>
        public static void PrintCustomers(IEnumerable<Customer> customers)
        {
            Console.WriteLine($"|{"CustomerId", 15}|{"FirstName", 20}|{"LastName", 20}|{"Country", 25}|{"PostalCode", 15}|{"Phone", 25}|{"Email", 40}|");
            Console.WriteLine(new string('-', 168));
            foreach (var customer in customers)
            {
                Console.WriteLine($"|{customer.Id, 15}|{customer.FirstName, 20}|{customer.LastName, 20}|{customer.Country, 25}|{customer.PostalCode, 15}|{customer.Phone, 25}|{customer.Email, 40}|");
            }
            Console.WriteLine(new string('-', 168));
            Console.WriteLine();
        }

        /// <summary>
        /// Print a table of one specific customer to the console.
        /// </summary>
        /// <param name="customer">Customer to print</param>
        public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"|{"CustomerId", 15}|{"FirstName", 20}|{"LastName", 20}|{"Country", 25}|{"PostalCode", 15}|{"Phone", 25}|{"Email", 40}|");
            Console.WriteLine(new string('-', 168));
            Console.WriteLine($"|{customer.Id, 15}|{customer.FirstName, 20}|{customer.LastName, 20}|{customer.Country, 25}|{customer.PostalCode, 15}|{customer.Phone, 25}|{customer.Email, 40}|");
            Console.WriteLine(new string('-', 168));
            Console.WriteLine();
        }
        
        /// <summary>
        /// Print a table of countries and a number of customers in each country to the console.
        /// </summary>
        /// <param name="customersCountry">Collection of CustomerCountry to print.</param>
        public static void PrintCustomersCountry(IEnumerable<CustomerCountry> customersCountry)
        {
            Console.WriteLine($"|{"CountryName", 25}|{"NumberOfCustomers", 25}|");
            Console.WriteLine(new string('-', 53));
            foreach (var customerCountry in customersCountry)
            {
                Console.WriteLine($"|{customerCountry.CountryName, 25}|{customerCountry.NumberOfCustomers, 25}|");
            }
            Console.WriteLine(new string('-', 53));
            Console.WriteLine();
        }

        /// <summary>
        /// Print a table of customers who are the highest spenders with their spending to the console.
        /// </summary>
        /// <param name="customersSpenders">Collection of CustomerSpender to print.</param>
        public static void PrintCustomersTopSpenders(IEnumerable<CustomerSpender> customersSpenders)
        {
            Console.WriteLine($"|{"CustomerName", 20}|{"HighestSpend", 20}|");
            Console.WriteLine(new string('-', 43));
            foreach (var customerSpender in customersSpenders)
            {
                Console.WriteLine($"|{customerSpender.CustomerName, 20}|{customerSpender.Spend, 20}|");
            }
            Console.WriteLine(new string('-', 43));
            Console.WriteLine();
        }
        
        /// <summary>
        /// Print a table of a customer and their the most popular genres to the console.
        /// </summary>
        /// <param name="customerGenres">Collection of CustomerGenre to print.</param>
        public static void PrintCustomerTopGenres(IEnumerable<CustomerGenre> customerGenres)
        {
            Console.WriteLine($"|{"CustomerName", 20}|{"MostPopularGenre", 20}|{"NumberOfInvoices",20}|");
            Console.WriteLine(new string('-', 64));
            foreach (var customerGenre in customerGenres)
            {
                Console.WriteLine($"|{customerGenre.CustomerName, 20}|{customerGenre.MostPopularGenre, 20}|{customerGenre.NumberOfInvoices, 20}|");
            }
            Console.WriteLine(new string('-', 64));
            Console.WriteLine();
        }
    }
}