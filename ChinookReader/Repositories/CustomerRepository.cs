﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccess.Models;
using DataAccess.Utils;

namespace DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Add customer to the table Customer of Chinook data base.
        /// </summary>
        /// <param name="customer">Customer to add.</param>
        public void AddCustomer(Customer customer)
        {
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) "
                          +"VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
                
                var command = new SqlCommand(sql, connection);
                // Not null fields
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Email", customer.Email);
                // Nullable fields
                command.Parameters.AddWithValue("@Country", (object)customer.Country ?? DBNull.Value);
                command.Parameters.AddWithValue("@PostalCode", (object)customer.PostalCode ?? DBNull.Value);
                command.Parameters.AddWithValue("@Phone", (object)customer.Phone ?? DBNull.Value);
                
                command.ExecuteNonQuery();
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        /// <summary>
        /// Get a customer by the customer id from Customer table.
        /// </summary>
        /// <param name="customerId">Id of the customer to get.</param>
        /// <returns>Customer.</returns>
        public Customer GetCustomerById(int customerId)
        {
            Customer customer = new Customer();
            
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " 
                          + "FROM Customer "
                          + "WHERE CustomerId = @CustomerId";
                
                var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", customerId);
                
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customer.Id = reader.GetInt32(0);
                    customer.FirstName = reader.GetString(1);
                    customer.LastName = reader.GetString(2);
                    customer.Country = SafeGetString(reader, 3);
                    customer.PostalCode = SafeGetString(reader, 4);
                    customer.Phone = SafeGetString(reader, 5);
                    customer.Email = reader.GetString(6);
                }
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            return customer;
        }

        /// <summary>
        /// Get a customer by the customer first name from Customer table.
        /// </summary>
        /// <param name="customerName">First name of the customer to get.</param>
        /// <returns>Customer.</returns>
        public Customer GetCustomerByName(string customerName)
        {
            Customer customer = new Customer();
            
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " 
                          + "FROM Customer "
                          + "WHERE FirstName = @FirstName";
                
                var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@FirstName", customerName);
                
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customer.Id = reader.GetInt32(0);
                    customer.FirstName = reader.GetString(1);
                    customer.LastName = reader.GetString(2);
                    customer.Country = SafeGetString(reader, 3);
                    customer.PostalCode = SafeGetString(reader, 4);
                    customer.Phone = SafeGetString(reader, 5);
                    customer.Email = reader.GetString(6);
                }
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            return customer;
        }

        /// <summary>
        /// Returns a list of all customers in the table Customer.
        /// </summary>
        /// <returns>List of customers.</returns>
        public IEnumerable<Customer> GetCustomers()
        {
            List<Customer> customers = new List<Customer>();
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
                var command = new SqlCommand(sql, connection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customers.Add(new Customer()
                    {
                        Id = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = SafeGetString(reader, 3),
                        PostalCode = SafeGetString(reader, 4),
                        Phone = SafeGetString(reader, 5),
                        Email = reader.GetString(6)
                    });
                }
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            return customers;
        }

        /// <summary>
        /// Return a page of customers with the given limit of customers and with an offset.
        /// </summary>
        /// <param name="limit">Number of customer to show.</param>
        /// <param name="offset">Number of customer to skip.</param>
        /// <returns>List of customers.</returns>
        public IEnumerable<Customer> GetPageOfCustomers(int limit, int offset)
        {
            var customers = new List<Customer>();
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email "
                            + "FROM Customer "
                            + "ORDER BY CustomerId "
                            + "OFFSET @offsetValue ROWS FETCH NEXT @limitValue ROWS ONLY";
                
                var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@offsetValue", offset);
                command.Parameters.AddWithValue("@limitValue", limit);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customers.Add(new Customer()
                    {
                        Id = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = SafeGetString(reader, 3),
                        PostalCode = SafeGetString(reader, 4),
                        Phone = SafeGetString(reader, 5),
                        Email = reader.GetString(6)
                    });
                }
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            return customers;
        }

        /// <summary>
        /// Get a list of customers who are the highest spenders (total in invoice table is the largest), ordered descending.
        /// </summary>
        /// <returns>List of customers names and the highest spend amount.</returns>
        public IEnumerable<CustomerSpender> GetCustomersTopSpenders()
        {
            var customersTopSpenders = new List<CustomerSpender>();
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "SELECT Customer.FirstName, SUM(Invoice.Total) AS HighestSpend "
                            + "FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId "
                            + "GROUP BY Customer.FirstName "
                            + "HAVING SUM(Total) = (SELECT MAX(Spend) AS MaxSpend FROM (SELECT CustomerId, SUM(Total) AS Spend FROM Invoice GROUP BY CustomerId) AS CustomerSpend) "
                            + "ORDER BY Customer.FirstName DESC";
                
                var command = new SqlCommand(sql, connection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customersTopSpenders.Add(new CustomerSpender()
                    {
                        CustomerName = reader.GetString(0),
                        Spend = reader.GetDecimal(1),
                    });
                }
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            return customersTopSpenders;
        }

        /// <summary>
        /// Get a list of counted customers by country.
        /// </summary>
        /// <returns>List of countries and number of customers from this country.</returns>
        public IEnumerable<CustomerCountry> GetCustomerCountByCountry()
        {
            var customersCountByCountry = new List<CustomerCountry>();
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "SELECT Country, COUNT(Country) as NumberOfCustomers "
                          + "FROM Customer "
                          + "GROUP BY Country "
                          + "ORDER BY NumberOfCustomers DESC";
                
                var command = new SqlCommand(sql, connection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customersCountByCountry.Add(new CustomerCountry()
                    {
                        CountryName = reader.GetString(0),
                        NumberOfCustomers = reader.GetInt32(1),
                    });
                }
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            return customersCountByCountry;
        }

        /// <summary>
        ///  Returns most popular genres by the customer.
        ///  (Most popular in this context means the genre that corresponds to the most tracks from invoices associated to that customer.)
        /// </summary>
        /// <param name="customerId">Customer Id of the customer of which you want to get most popular genres.</param>
        /// <returns>List of the most popular genres for the customer.</returns>
        public IEnumerable<CustomerGenre> GetCustomerTopGenres(int customerId)
        {
            var customerTopGenres= new List<CustomerGenre>();
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "SELECT TOP 1 WITH TIES "
                            + "Customer.FirstName,"
                            + "Genre.[Name] as GenreName,"
                            + "COUNT(*) as NumberOfInvoices "
                            + "FROM InvoiceLine "
                            + "INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId "
                            + "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId "
                            + "INNER JOIN Genre ON Genre.GenreId = Track.GenreId "
                            + "INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId "
                            + "WHERE Customer.CustomerId = @CustomerId "
                            + "GROUP BY Customer.FirstName, Genre.[Name] "
                            + "ORDER BY NumberOfInvoices DESC";
                
                var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", customerId);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    customerTopGenres.Add(new CustomerGenre()
                    {
                        CustomerName = reader.GetString(0),
                        MostPopularGenre = reader.GetString(1),
                        NumberOfInvoices = reader.GetInt32(2)
                    });
                }
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            return customerTopGenres;
        }

        /// <summary>
        /// Update all customer attributes by new customer data.
        /// </summary>
        /// <param name="customer"></param>
        public void UpdateCustomer(Customer customer)
        {
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
            var existingCustomer = GetCustomerById(customer.Id);
            
            try
            {
                connection.Open();
            
                var sql = "UPDATE Customer SET " 
                          + "FirstName = @FirstName,"
                          + "LastName = @LastName,"
                          + "Country = @Country,"
                          + "PostalCode = @PostalCode,"
                          + "Phone = @Phone,"
                          + "Email = @Email "
                          + "WHERE CustomerId = @CustomerId";
                
                var command = new SqlCommand(sql, connection);

                // Check existing customer fields that they have values
                var finalFirstName = customer.FirstName ?? existingCustomer.FirstName;
                var finalLastName = customer.LastName ?? existingCustomer.LastName;
                var finalEmail = customer.Email ?? existingCustomer.Email;
                var finalCountry = customer.Country ?? existingCustomer.Country;
                var finalPostalCode = customer.PostalCode ?? existingCustomer.PostalCode;
                var finalPhone = customer.Phone ?? existingCustomer.Phone;
                
                // Not null fields
                command.Parameters.AddWithValue("@CustomerId", customer.Id);
                command.Parameters.AddWithValue("@FirstName", finalFirstName);
                command.Parameters.AddWithValue("@LastName", finalLastName);
                command.Parameters.AddWithValue("@Email", finalEmail);
                
                // Nullable fields
                command.Parameters.AddWithValue("@Country", (object)finalCountry ?? DBNull.Value);
                command.Parameters.AddWithValue("@PostalCode", (object)finalPostalCode ?? DBNull.Value);
                command.Parameters.AddWithValue("@Phone", (object)finalPhone ?? DBNull.Value);
                
                command.ExecuteNonQuery();
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        /// <summary>
        /// Delete customer from Customer table by the given Id.
        /// </summary>
        /// <param name="customerId">Customer Id.</param>
        public void DeleteCustomer(int customerId)
        {
            using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());

            try
            {
                connection.Open();
            
                var sql = "DELETE FROM Customer "
                          + "WHERE CustomerId = @CustomerId";
                
                var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", customerId);

                command.ExecuteNonQuery();
            }
            catch (SqlException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        /// <summary>
        /// Wrapper function for GetString function to handle nullable fields when trying to get a field as a string.
        /// </summary>
        /// <param name="reader">Sql reader.</param>
        /// <param name="colIndex">Index of a table column to convert.</param>
        /// <returns>Converted to a string table field or an empty string if the field was null.</returns>
        private string SafeGetString(IDataRecord reader, int colIndex)
        {
            return reader.IsDBNull(colIndex) ? string.Empty : reader.GetString(colIndex);
        }
    }
}