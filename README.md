<div align="center">
    <h1>Noroff Assignment 5: Data Persistence and Access</h1>
    <img src="https://i.ibb.co/cxxNpF2/sql.png" width="128" alt="SQL">
</div>

[![license](https://img.shields.io/badge/License-MIT-green.svg)](LICENSE)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Chinook and Superheroes

For this group project we had to work on database access and communication. The project consists of two separate folders/projects, this is how we, as a group of two, split our work. 
1. The **ChinookReader** folder contains a C# console application which connects to a local database (**Chinook**) and executes **CRUD** queries. Chinook models iTunes database of customers purchasing songs. The app also uses the implementation of the repository pattern and it's used to interact with the database.
2. The **SQL_Supereroes** folder contains 9 separate files, which are sequential SQL queries, as well as 1 general file, which includes all individual queries. These queries allow you to create a database, setup some tables in the database, add relationships to the tables, and then populate the tables with data. The database and its theme are surrounding **superheroes**.

A complete list of all requirements is given in PDF (**Assignment_requirements.pdf**).

Created by Ayoub Auolad ali & Lev Nagornov.

## Table of Contents

- [Security](#security)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Security

We make use of the SQL dependency method when executing database communication functions. This provides a secure communication and can prevent an SQL injection attack.

## Install
Download and install: 
* [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)
* [.Net 5.0 or later](https://dotnet.microsoft.com/en-us/download/dotnet)

Clone the repository using:

```
git clone git@gitlab.com:AyoubAuolad/csharp_data_access.git
```

## Usage

### Chinook Reader

1. Create **Chinook** database by executing the **Chinook_SqlServer_AutoIncrementPKs.sql** file with **SQL Server Management Studio**.

2. Navigate to **ChinookReader/Utils/** and open **ConnectionHelper.cs** file and change **DataSource** to your own server name.  
  You can find this name in **SQL Server Management Studio**:
    1. Right click on your server inside the **Object Explorer**
    2. Click on **Properties**
    3. Copy the value of the **Name** field

Now you can run the **Program.cs** file to see the output of the different queries. All changes are also reflected within the actual database.

### SQL Superheroes

* Run all numbered files one by one OR run one common file containing all queries.
    * If you decide to run the common file, open **Single-Query-CreateDB.sql** file with **SQL Server Management Studio** and click **Execute**.
    <br>OR<br>
    * If you decide to run numbered files, open **01_dbCreate.sql** file with **SQL Server Management Studio** and click **Execute**, then repeat it with the other numbered files.

## Maintainers

[@AyoubAuolad](https://gitlab.com/AyoubAuolad)\
[@lnagornov](https://gitlab.com/lnagornov)

## Contributing

PRs accepted.

Small note: If editing the Readme, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

[MIT © Ayoub Auolad ali, Lev Nagornov.](../LICENSE)
